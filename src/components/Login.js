import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
  COLOR_PINK,
  COLOR_PINK_LIGHT,
  COLOR_PINK_MEDIUM,
  COLOR_FACEBOOK,
} from '../styles/valueColors';

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  }
  render() {
    const Divider = (props) => {
      return <View {...props}>
        <View style={styles.line}></View>
        <Text style={styles.textOR}>OR</Text>
        <View style={styles.line}></View>
      </View>
    }
    return (
      <View style={styles.container}>
        <View style={styles.up}>
          <Ionicons
            name="ios-speedometer"
            size={100}
            color={COLOR_PINK}>
          </Ionicons>
          <Text style={styles.title}>
            Share your images for everyone
        </Text>
        </View>
        <View style={styles.down}>
          <View style={styles.textInputContainer}>
            <TextInput
              style={styles.textInput}
              textContentType='emailAddress'
              keyboardType='email-address'
              placeholder='Enter your email'
              ></TextInput>
          </View>
          <View style={styles.textInputContainer}>
            <TextInput
              style={styles.textInput}
              placeholder='Enter your password'
              secureTextEntry={true}
            ></TextInput>
          </View>
          <TouchableOpacity
            style={styles.loginButton}
          >
            <Text style={styles.loginButtonTitle}>LOGIN</Text>
          </TouchableOpacity>
          <Divider style={styles.divider}></Divider>
          <FontAwesome.Button
            style={styles.facebookButton}
            name='facebook'
            backgroundColor={COLOR_FACEBOOK}
          >
            <Text style={styles.loginButtonTitle}>Login with Facebook</Text>
          </FontAwesome.Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: COLOR_PINK_LIGHT,
  },
  up: {
    flex: 3,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  down: {
    paddingTop: 20,
    flex: 7,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    color: COLOR_PINK_MEDIUM,
    textAlign: 'center',
    width: 400,
    fontSize: 23,
  },
  textInputContainer: {
    paddingHorizontal: 10,
    backgroundColor: 'rgba(255,255,255,0.2)',
    borderRadius: 6,
    marginBottom: 20,
  },
  textInput: {
    width: 280,
    height: 45,
  },
  loginButton: {
    width: 300,
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR_PINK,
  },
  loginButtonTitle: {
    fontSize: 18,
    color: 'white',
  },
  facebookButton: {
    width: 300,
    height: 45,
    justifyContent: 'center',
  },
  line: {
    height: 1,
    flex: 2,
    backgroundColor: 'black',
  },
  textOR: {
    flex: 1,
    textAlign: 'center',
  },
  divider: {
    flexDirection: 'row',
    width: 298,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
